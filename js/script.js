class ShoppingCart {
	constructor(navigation, cart) {
		this.cart = cart;
		this.navigation = navigation;
		this.products = [
			{
				id: '1',
				category: 'T-Shirts',
				name: 'Green-Xtreame',
				price: 350
			},
			{
				id: '2',
				category: 'T-Shirts',
				name: 'Digital Owl',
				price: 312
			},
			{
				id: '3',
				category: 'T-Shirts',
				name: 'I can',
				price: 280
			},
			{
				id: '4',
				category: 'T-Shirts',
				name: 'GB shirts',
				price: 320
			}
		];
		this.$pageProduct = document.querySelector('.page-product');
		this.$pageIndex = document.querySelector('.page-index');
		this.$tbody = document.querySelector('table tbody');
		this.$butClearAll = document.querySelector('.clear-all-but');
		this.$arrayOfAmounts = document.querySelectorAll('.amount');
		this.$butPlusProduct = document.querySelectorAll('.page-product .but-plus');
		this.$totalSum = document.querySelector('.sum-products');
		this.$butBuy = document.querySelector('.but-buy');
        
		this.$pageProduct.addEventListener('click', this.handleOperation.bind(this));
		this.$pageIndex.addEventListener('click', this.handleOperation.bind(this));
		this.$butClearAll.addEventListener('click', this.clearAll.bind(this));
		this.$tbody.addEventListener('click', this.cartOperation.bind(this));
		this.$tbody.addEventListener('click', this.removeRow.bind(this));
		this.$butBuy.addEventListener('click', this.buyOperation.bind(this));

		this.drawAmountProduct();
	}

	handleOperation(event) {
		const target = event.target;
		const $parentEl = target.parentNode;
		const href = event.target.dataset.href;

		let elemId = target.dataset.productid,
		$amountEl = $parentEl.children[1],
		product = this.products.find((product) => product.id === elemId);
		if (target.classList.contains('but-plus')) {
			this.cart.addToCart(product);
			this.drawAmountProduct(target);
		} else if (target.classList.contains('but-minus')) {
			this.cart.removeFromCard(product);
			this.drawAmountProduct(target);
		} else if (href === 'page-cart') {
			this.drawTable();
			this.sumProducts();
		}
	}
	drawAmountProduct(clickEl){
		/*Коли ми виконуємо операцію "додавання" або 
		"віднімання" перемальовуємо кiлькість в категорії вибіру даного товара*/
		const productsInCart = this.cart.getProducts();
		if(clickEl) {
			const elemId = clickEl.dataset.productid;
			const $parentBlock = clickEl.parentNode;
			let check = false;
			for(let elem of $parentBlock.children) {
				if(elem.classList.contains('info')) {
					for(let amount of elem.children) {
						if(amount.classList.contains('amount')) {
							for(let product of productsInCart) {
								if(product.id ===  elemId) {
									amount.innerText = product.amount;
									check = true;
									return
								} else {
									check = false;
								}
							}
							if(check === false) {
								amount.innerText = 0;
							}
						}
					}
				}
			}
		} else {
			for(let elem of this.$butPlusProduct) {
				for(let product of productsInCart) {
					if(elem.dataset.productid === product.id) {
						const $parentEl = elem.parentNode;
						const $blockInfo = $parentEl.children[1];
						const $amountEl = $blockInfo.children[1];
						$amountEl.innerText = product.amount;
					}
				}
			}
		}	
	}
	drawTable(){
		this.$tbody.innerHTML = '';
		const productsInCart =  this.cart.getProducts();
		for (let i = 0; i<productsInCart.length; i++){
			const $row = this.drawRow(productsInCart[i], i);
			this.$tbody.appendChild($row);
		}
	}
	drawRow(row, index){
		const rowTemplate = `
		<td>
			<button class="button-delete" rowindex= ${index}></button>
			<span class="name-product">${row.name}</span>
		</td>
		<td>
			<div class="operations">
				<button class="but-minus" data-productid=${row.id}>-</button>
				<div class="info">
					<span class="amount">${row.amount}</span>
				</div>
				<button class="but-plus" data-productid=${row.id}>+</button>
			</div>
		</td>
		<td class="product-total-price">${row.price*row.amount}</td>`;

		const tr = document.createElement('tr');
		tr.innerHTML = rowTemplate;
		return tr;
	}
	clearAll(){
		this.$tbody.innerHTML = '';
		this.cart.removeAllProducts();
		this.resetAmount();
	}
	resetAmount(){
		/* Коли ми натискаємо кнопку clearAll в корзині. Обнуляємо кількість,
		на сторінці 'вибіру товарів'*/
		for(let amount of this.$arrayOfAmounts){
			amount.innerText = 0;
		}
	}
	sumProducts(){
		//сумує ціну всіх продуктів і малює 
		const productsInCart = this.cart.getProducts();
		const productsSum = productsInCart.reduce((sum, current)=>{
			return sum += current.amount*current.price;
		}, 0);
		this.drawTotalSum(productsSum);
		
		return productsSum;
	}
	drawTotalSum(count){
		this.$totalSum.innerText = count;
	}
	removeRow(){
		//видалення рядка
		if(event.target.classList.contains('button-delete')) {
			const id = event.target.getAttribute('rowIndex');
			this.cart.deleteProductById(id);
			this.drawTable();
			this.sumProducts();
		}
	}
	cartOperation(event){
		//Операції додвання і віднімання товарів в корзині
		const target = event.target;
		//const $parentEl = target.parentNode;
		let elemId = target.dataset.productid,
		//$amountEl = $parentEl.children[0],
		product = this.products.find((product) => product.id === elemId);

		if (target.classList.contains('but-plus')) {
			this.cart.addToCart(product);

			this.drawTable();
			this.drawAmountProduct(target);
			this.sumProducts();
			this.redrawAmount(product);
		} else if (target.classList.contains('but-minus')) {
			this.cart.removeFromCard(product);

			this.drawTable();
			this.drawAmountProduct(target);
			this.sumProducts();
			this.redrawAmount(product);
		}
	}
	redrawAmount(product) {
		//Перемальовуємо кількість товарів на сторінці "вибіру товарів""
		const productId = product.id;
		const productsInCart = this.cart.getProducts();
		let check = false;
		for(let product of productsInCart) {
			if(product.id === productId) {
				this.$arrayOfAmounts[+productId-1].innerText = product.amount;
				check = true;
				return
			} else {
				check = false;
			}
		} if(check === false) {
			this.$arrayOfAmounts[+productId-1].innerText = 0;
		}	
	}
	buyOperation() {
		const sum = this.sumProducts();
		alert('Сума вашої покупки складає: ' + sum);
		this.clearAll();
	}
}
class Navigation {
	constructor() {
		this.currentPage;
		this.$body = document.querySelector('body');
		this.$pages = document.querySelectorAll('.page');
		this.$body.addEventListener('click',this.navigate.bind(this));
	}
	navigate(event) { 
		const href = event.target.dataset.href;
		if(href && href !== this.currentPage) {
			const pageNavigateTo = document.querySelector(`.${href}`);
			for(let page of this.$pages) {
				page.style.display = 'none';
			}
			if(pageNavigateTo.classList.contains('page-cart')) {
				pageNavigateTo.style.display = 'block';
			}else{
				pageNavigateTo.style.display = 'block';
			}
		}
	}
}
class Cart {
	constructor() {
		this.storage = {
			items: [],
			get products() {
				if(window.localStorage.products) {
		    		this.items = JSON.parse(localStorage.getItem('products'));
				} else {
					//window.localStorage.products = [];
					localStorage.setItem('products', JSON.stringify([]));
					this.items = [];
				}
		    	return this.items;
		  	},
		  	set products(value) {
		    	//this.items = value;
		    	localStorage.setItem('products', JSON.stringify(value));
		    	console.log('set');
		  	}
		};
	}
	addToCart(product) {
		let productInCard = this.storage.products.find((item) => item.id === product.id);
  		// додаєм в корзину
    	if(productInCard) {
    	   productInCard.amount++;
    	   const indexProduct = this.storage.products.findIndex((p) => p.id === productInCard.id);
    	   const array = [...this.storage.products];
		   array.splice(indexProduct, 1, productInCard);
		   this.storage.products = array;
    	} else {
			this.storage.products = [...this.storage.products, {...product, amount: 1}];
		} 
	}
	removeFromCard(product) {
		let productInCard = this.storage.products.find((item) => item.id === product.id);
		if(productInCard && productInCard.amount > 1) {
    		productInCard.amount--;

    		const indexProduct = this.storage.products.findIndex((p) => p.id === productInCard.id);
    	   	const array = [...this.storage.products];

			array.splice(indexProduct, 1, productInCard);
		   	this.storage.products = array;    	   	
    	}  else {
    		let indexEl = this.storage.products.findIndex((p) => p.id === productInCard.id);
    		const array = [...this.storage.products];

      		array.splice(indexEl, 1);
      		this.storage.products = array;
    	}
	}
	removeAllProducts() {
		this.storage.products = [];
	}
	getProducts() {
 		return this.storage.products;
	}
	deleteProductById(id) {
		this.storage.products.splice(id, 1);
	}
}
document.addEventListener('DOMContentLoaded',()=> {
	new ShoppingCart(
		new Navigation,
		new Cart
	);
});
